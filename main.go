package main

import (
	"github.com/go-redis/redis"
	"os"
	"log"
	"github.com/gin-gonic/gin"
)

const (
	KEY = "rds-test-key"
	VALUE = "rds-test-value"
)

func main() {
	addr := os.Getenv("REDIS_ADDR")
	if addr == "" {
		addr = "localhost:6379"
	}

	c := redis.NewClient(&redis.Options{
		Addr: addr,
		DB: 0,
	})

	err := c.Set(KEY, VALUE, 0).Err()

	if err != nil {
		log.Fatal(err)
	}

	r := gin.Default()
	r.GET("/", func(cc *gin.Context) {
		val, err := c.Get(KEY).Result()
		if err != nil {
			log.Printf("get key %s error\n", KEY)
			cc.String(200, "error")
			return
		}

		cc.String(200, val)
	})

	r.Run()
}

