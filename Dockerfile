FROM zcong/golang:1.10.3 AS build
WORKDIR /go/src/github.com/zcong1993/rds-test
COPY . .
RUN dep ensure -vendor-only -v && \
    CGO_ENABLED=0 go build -o ./bin/rds-test main.go

FROM alpine:3.7
WORKDIR /opt
EXPOSE 8080
RUN apk add --no-cache bash git openssh
COPY --from=build /go/src/github.com/zcong1993/rds-test/bin/* /usr/bin/
CMD ["rds-test"]
